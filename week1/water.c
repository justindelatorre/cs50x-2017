// #include <cs50.h>
#include <stdio.h>

int main(void)
{
    printf("How many minutes do you shower on average? ");
    float minutes = get_float();

    printf("You used %.2f bottles of water!\n", minutes * 1.5 / 16);
}
