#include <cs50.h>
#include <stdio.h>

void print_spaces(int h, int i);
void print_blocks(int i);

int main(void)
{
    // Get a number 23 or less
    printf("Enter an integer no greater than 23: ");
    int height = get_int();

    // Check if number is valid
    while (height < 0 || height > 23)
    {
        printf("That's not a valid number. Please enter another one: ");
        height = get_int();
    }

    // Create rows based on height value
    for (int i = 1; i < height + 1; i++)
    {
        // Print number of spaces, which is height - blocks
        print_spaces(height, i);
        
        // Print number of blocks for this iteration
        print_blocks(i);
        
        // Print gap (always two spaces)
        printf("  ");
        
        // Print number of blocks again
        print_blocks(i);
        
        // Print number of spaces, which is height - blocks
        print_spaces(height, i);
        
        // Insert line break
        printf("\n");
    }
    
    return 0;
}

void print_spaces(int h, int i)
{
    for (int s = 0; s < h - i; s++)
        printf(" ");
}

void print_blocks(int i)
{
    for (int b = 0; b < i; b++)
        printf("#");
}
